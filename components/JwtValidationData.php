<?php
namespace app\components;

class JwtValidationData extends \sizeg\jwt\JwtValidationData
{
 
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->validationData->setIssuer('https://ssl.gcl-intl.com');
        $this->validationData->setAudience('https://ssl.gcl-intl.com');
        $this->validationData->setId('4f1g23a12aa121255555123TysnQgfRTsdDRRsdf');

        parent::init();
    }
}    
?>